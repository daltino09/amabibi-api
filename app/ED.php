<?php
/**
 * Created by PhpStorm.
 * User: angelpilot5
 * Date: 5/20/17
 * Time: 1:51 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ED extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ED';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'LEID', 'LEWord', 'LEPro','LEPos', 'LEDef', 'LEImg','LEAFile', 'UID', 'LEDateUpdated', 'LEDateAdded','deleted_at','created_at','updated_at'
    ];

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}