<?php
/**
 * Created by PhpStorm.
 * User: angelpilot5
 * Date: 5/20/17
 * Time: 1:46 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class WGroup extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'LWordGroup';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'LWGID', 'LID','UID', 'EWG', 'EWGDescription','LWG', 'LWGDateUpdated', 'LWGDateAdded','LWGCategory', 'LWGApprove','LWLevel', 'EWGAFile', 'LWGAFile', 'created_at','deleted_at','updated_at'
    ];

    public function kWord()
    {
        return $this->belongsTo('App\KalD');
    }

    public function scopeActive($query)
    {
        return $query->where('LWGApproved', 1);
    }
}