<?php
/**
 * Created by PhpStorm.
 * User: angelpilot5
 * Date: 5/20/17
 * Time: 1:46 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class KalD extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'KalD';


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'LWID', 'LEID', 'LWord', 'LWDescription','LWImg', 'LWAFile', 'LWApproved','LWCategory', 'LWLevel', 'UID', 'LWDateUpdated', 'LWDateAdded'
    ];

    public function english()
    {
        return $this->belongsTo('App\ED');
    }

    public function scopeActive($query)
    {
        return $query->where('LWApproved', 1);
    }
}