<?php
/**
 * Created by PhpStorm.
 * User: angelpilot5
 * Date: 5/20/17
 * Time: 1:59 AM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Request;
use App\KalD;
use App\ED;
use App\WGroup;
use Symfony\Component\HttpFoundation\Session\Storage;

class LangController extends Controller
{
    public function index(){
        return 'Amabibi API V1';
    }

    public function getWord(Request $request){
        $input = $request->all();
        $lang = $input['lang'];
        $lwid = $input['lwid'];
        $error = '';
        if($lang == 'KalD')
            $word = KalD::find($lwid);
        else
            $error = 'Language not found!';

        $request = ['error'=>$error,'data'=>$word];
        return json_encode($request);
    }

    public function listWords($lang='KalD'){
        $error = '';
        if($lang == 'KalD')
            $words = KalD::all();
        else
            $error = 'Language not found';

        $request = ['error'=>$error,'data'=>$words];
        return json_encode($request);
    }

    public function listWordsBatch($lang='KalD',$count=100,$lastId=0){
        $error = '';
        if($lang == 'KalD'){
            $words = KalD::where('LWID','>',$lastId)->take($count)->orderBy('LWord','asc')->get();
            foreach($words as $word){
                $eword = ED::where('LEID',$word->LEID)->first();
                $word->eword = $eword->LEWord;
                $word->ewordpos = $eword->LEPos;
                $word->eworddesc = $eword->LEDef;
            }
        }
        else
            $error = 'Language not found';

        $request = ['error'=>$error,'data'=>$words];
        return json_encode($request);
    }

    public function listWordGroupsBatch($lang='KalD',$count=100,$lastId=0){
        $error = '';
        if($lang == 'KalD'){
            $wgs = WGroup::where('LWGID','>',$lastId)->take($count)->get();
        }
        else
            $error = 'Language not found';

        $request = ['error'=>$error,'data'=>$wgs];
        return json_encode($request);
    }

    public function storeWord(Request $request){

    }

    public function updateWord(Request $request){

    }

    public function deleteWord(Request $request){
        $input = $request->all();
        $lang = $input['lang'];
        $lwid = $input['lwid'];
        $error = ''; $msg = 'failed';
        if($lang == 'KalD'){
            $word = KalD::find($lwid);
            $word->delete();
            $msg = 'success';
        }
        else
            $error = 'Language not found!';

        $request = ['error'=>$error,'msg'=>$msg];
        return json_encode($request);
    }

    public function getWordOfTheDay($lang = 'KalD'){
        $error = '';
        $word = '';
        if($lang == 'KalD'){
            //get the kword
            $notFound = true;
            while($notFound){
                $word = KalD::all()->random(1)->first();
                if($word->LEID){
                    $eword = ED::where('LEID',$word->LEID)->first();
                    if($eword){
                        $notFound = false;
                        $word->eword = $eword->LEWord;
                        $word->ewordpos = $eword->LEPos;
                        $word->eworddesc = $eword->LEDef;
                        $word->wgroups = $this->getWordGroupOfTheDay($word->LWord,'KalD');
                    }
                }    
            }
        }
        else
            $error = 'Language not found!';

        $request = ['error'=>$error,'data'=>$word];
        echo json_encode($request);
    }

    public function getWordGroupOfTheDay($word,$lang = 'KalD'){
        $error = '';
        if($lang == 'KalD'){
            $wgroup = WGroup::where('LWG','LIKE','%'.$word.'%')->get();
            if(!$wgroup && !$wgroup->count() > 0){
                $wgroup = WGroup::all()->random(1)->first();
            } else if($wgroup && $wgroup->first()){
                $wgroup = $wgroup->random(1)->first();
            }
        }else{
            $error = 'Language not found';
        }
        $request = ['error'=>$error,'data'=>$wgroup];
        return $request;
    }

    public function getTrackAudio($audio){

        $file=Storage::disk('local')->get($audio);
        $fileName= $audio;
        $filesize = Storage::disk('local')->size($fileName);


        // return response($file, 200)->header('Content-Type', $mime_type);

        $size   = $filesize;       // File size
        $length = $size;           // Content length
        $start  = 0;               // Start byte
        $end    = $size - 1;       // End byte

        return response($file)
            ->withHeaders([
                'Accept-Ranges' => "bytes",
                'Accept-Encoding' => "gzip, deflate",
                'Pragma' => 'public',
                'Expires' => '0',
                'Cache-Control' => 'must-revalidate',
                'Content-Transfer-Encoding' => 'binary',
                'Content-Disposition' => ' inline; filename='.$audio,
                'Content-Length' => $filesize,
                'Content-Type' => "audio/mpeg",
                'Connection' => "Keep-Alive",
                'Content-Range' => 'bytes 0-'.$end .'/'.$size,
                'X-Pad' => 'avoid browser bug',
                'Etag' => $audio,
            ]);



    }
}