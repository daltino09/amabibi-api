<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$app->get('/', function () use ($app) {
//    return $app->version();
//});

$app->get('/', array('middleware'=>'cors','uses' => 'LangController@index'));
$app->get('/list-words/{lang}', array('middleware'=>'cors','uses' => 'LangController@listWords'));
$app->get('/list-words-batch/{lang}/{count}/{lastId}', array('middleware'=>'cors','uses' => 'LangController@listWordsBatch'));
$app->post('/get-word', array('middleware'=>'cors','uses' => 'LangController@getWord'));
$app->post('/delete-word', array('middleware'=>'cors','uses' => 'LangController@deleteWord'));
$app->get('/get-word-of-the-day/{lang}', array('middleware'=>'cors','uses' => 'LangController@getWordOfTheDay'));